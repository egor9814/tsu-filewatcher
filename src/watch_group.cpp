//
// Created by egor9814 on 13 Nov 2020.
//

#include "watch_group.hpp"
#include <file_watcher/file_stat.hpp>
#include <threading/safe_cout.hpp>

void WatchGroup::attachConsole(fw::FileWatcher *watcher) {
	// присоединияем консоль, если это необходимо
	if (consoleOut) {
		watcher->attach(&console);
	}
}

void WatchGroup::attachLogger(fw::FileWatcher *watcher) {
	if (!logger) {
		// создаем логгер, если это необходимо
		if (!logfilePath.isEmpty()) {
			logger = std::make_shared<fw::file_observers::LogfileFileObserver>(logfilePath);
			if (!logger->open()) {
				auto cout = threading::cout::lock();
				auto &out = cout.get();
				out << "warning: cannot open log file '"
					<< logfilePath.toStdString() << "'" << std::endl;
				logger.reset();
			}
		}
	}
	// если есть логгер, то присоединяем его
	if (logger) {
		watcher->attach(logger.get());
	}
}

bool WatchGroup::watch(const QString &filePath) {
	// создаем наблюдателя
	auto watcher = std::make_shared<fw::FileWatcher>(filePath, updatePeriodTime, watchTime);
	// происоединяем консоль и логгера
	attachConsole(watcher.get());
	attachLogger(watcher.get());
	// добавляем в карту наблюдателей
	watchers[watcher->getPath()] = watcher;
	// пытаемся запустить наблюдение
	if (!watcher->watch()) {
		// если не получилось, то удаляем наблюдателя из карты
		watchers.remove(watcher->getPath());
		// и возвращаем false
		return false;
	}
	// успех!!!
	return true;
}

void WatchGroup::stop() {
	// останавливаем всех наблюдателей
	for (auto &it : watchers) {
		it->stop();
	}
}

void WatchGroup::wait() {
	// ждем всех наблюдателей
	for (auto &it : watchers) {
		it->await();
	}
}

WatchGroup *WatchGroup::instance() {
	// singleton
	static WatchGroup wg;
	return &wg;
}

// объявляем метатип, чтобы можно было его передавать в качестве аргумента в SIGNAL->SLOT
using FS = fw::FileStat;
Q_DECLARE_METATYPE(FS)

WatchGroup::WatchGroup() {
	// а также регистрируем его
	qRegisterMetaType<FS>("FileStat");
}
