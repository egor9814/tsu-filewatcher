//
// Created by egor9814 on 13 Nov 2020.
//

#ifndef FILEWATCHER_WATCH_GROUP_HPP
#define FILEWATCHER_WATCH_GROUP_HPP

#include <file_watcher/watcher.hpp>
#include <file_watcher/console_file_observer.hpp>
#include <file_watcher/logfile_file_observer.hpp>
#include <memory>
#include <QMap>

/// Структура группы наблюдателей
struct WatchGroup {
	/// Время наблюдения и период обновления
	uint64_t watchTime{0}, updatePeriodTime{100};
	/// Признак вывода на консоль
	bool consoleOut{true};
	/// Путь к log-файлу
	QString logfilePath;

private:
	/// Наблюдатель статистики с выводом на консоль
	fw::file_observers::ConsoleFileObserver console;
	/// Наблюдатель статистики с выводом в log-файл
	std::shared_ptr<fw::file_observers::LogfileFileObserver> logger;

	/// Карта(словарь) наблюдателей
	QMap<QString, std::shared_ptr<fw::FileWatcher>> watchers;

	/// Метод для присоединения консоли
	void attachConsole(fw::FileWatcher *watcher);

	/// Метод для присоединения логгера
	void attachLogger(fw::FileWatcher *watcher);

public:
	/** Метод для запуска наблюдения
	 * @param filePath Путь к наблюдаемому файлу
	 * @return true, если удалось запустить процесс наблюдения, иначе false
	 * */
	bool watch(const QString &filePath);

	/// Метод для остановки наблюдателей
	void stop();

	/// Метод для ожидания завершения наблюдателей
	void wait();

	/// Метод для получения экземпляра группы наблюдателей
	static WatchGroup *instance();

private:
	/// Singleton
	WatchGroup();
	~WatchGroup() = default;
};

#endif //FILEWATCHER_WATCH_GROUP_HPP
