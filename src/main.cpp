#include <csignal>
#include <file_watcher/semver.hpp>
#include <threading/safe_cout.hpp>
#include "watch_group.hpp"

/** Функция для печати справки к програме
 * @param program Путь к исполняемому файлу
 * */
int printHelp(const char *program) {
	using std::endl;
	auto cout = threading::cout::lock();
	auto &out = cout.get();
	out
		<< "usage:"
		<< "  " << program << " {OPTION | FILE}+" << endl
		<< "  options:" << endl
		<< "    -h, --help          Print this help" << endl
		<< "    -v, --version       Print program version" << endl
		<< "    -p, --period PER    Set update period in milliseconds (>= 100ms, by default = 100)" << endl
		<< "    -t, --time TIME     Set watching time in seconds (for infinite watch use 0, by default = 0)" << endl
		<< "    -n, --no-console    Disable console output for file state" << endl
		<< "    -c, --console       Enable console output for file state (by default)" << endl
		<< "    -l, --log FILE      Set log file for file state" << endl
		<< endl;
	return 0;
}

/// Функция для печати версии программы
int printVersion() {
	using std::endl;
	auto cout = threading::cout::lock();
	auto &out = cout.get();
	out
		<< "FileWatcher by egor9814 v"
		<< FW_VERSION_MAJOR << '.' << FW_VERSION_MINOR << '.' << FW_VERSION_PATCH << endl
		<< "  Qt Version: "
		<< QT_VERSION_MAJOR << '.' << QT_VERSION_MINOR << '.' << QT_VERSION_PATCH << endl
		<< endl;
	return 0;
}

/// Функция для обработки сигналов внешнего завершения программы
void onSignal(int) {
	{
		using std::endl;
		auto cout = threading::cout::lock();
		auto &out = cout.get();
		// сообщаем что программа завершается
		out << endl << "<stopping watchers...>" << endl;
	}
	// останавливаем наблюдателей
	WatchGroup::instance()->stop();
}

int main(int argc, char **argv) {
	using std::endl;

	// регистрируем сигналы завершения
	signal(SIGINT, onSignal); // Ctrl+C
	signal(SIGTERM, onSignal);

	// если не достаточно аргументов, то отобразим справку
	if (argc == 1) {
		printHelp(argv[0]);
		return 1;
	}

	// получим экземпляр группы наблюдателей
	auto wg = WatchGroup::instance();

	// флаг, показывающий что получен неизвестный аргумент
	bool hasUnknownOptions = false;
	// проверочный цикл
	for (int i = 1; i < argc; i++) {
		auto arg = QString::fromUtf8(argv[i]);
		if (arg == "-h" || arg == "--help") {
			// печатаем справку
			return printHelp(argv[0]);
		} else if (arg == "-v" || arg == "--version") {
			// печатаем версию
			return printVersion();
		} else if (arg == "-l" || arg == "--log") {
			// устанавливем путь к log-файлу
			if (++i == argc) {
				auto cout = threading::cout::lock();
				auto &out = cout.get();
				out << "error: log file not specified" << endl;
				return 1;
			}
			wg->logfilePath = QString::fromUtf8(argv[i]);
		} else if (arg == "-p" || arg == "--period" ||
					arg == "-t" || arg == "--time") {
			// периодом обновления и время наблюдения могут применяться неоднократно,
			// поэтому пропускаем их в текущем цикле
			i++;
		} else if (arg != "-n" && arg != "--no-console" &&
					arg != "-c" && arg != "--console") {
			// вкл/выкл печати на консоль могут применяться неоднократно,
			// поэтому пропускаем их в текущем цикле
			if (arg.startsWith('-')) {
				// если аргумент начинается с минуса, то это опция,
				// известные опции обработали, значит эта опция неизвестная
				auto cout = threading::cout::lock();
				auto &out = cout.get();
				out << "error: unrecognized option '" << argv[i] << "'" << endl;
				hasUnknownOptions = true;
			}
		}
	}
	if (hasUnknownOptions) {
		// если есть неизвестные опции, то напечатаем справку
		printHelp(argv[0]);
		return 1;
	}

	// основной цикл запуска и конфигурации наблюдения
	for (int i = 1; i < argc; i++) {
		auto arg = QString::fromUtf8(argv[i]);
		// log-файл, справку и версию пропускаем
		if (arg == "-h" || arg == "--help" ||
			arg == "-v" || arg == "--version") continue;
		else if (arg == "-l" || arg == "--log") {
			i++;
			continue;
		}

		if (arg == "-p" || arg == "--period") {
			// устанавливем период обновления
			if (++i == argc) {
				auto cout = threading::cout::lock();
				auto &out = cout.get();
				out << "error: update period not specified" << endl;
				return 1;
			}
			bool ok = false;
			wg->updatePeriodTime = QString::fromUtf8(argv[i]).toULongLong(&ok);
			if (!ok) {
				auto cout = threading::cout::lock();
				auto &out = cout.get();
				out << "error: cannot parse update period value '" << argv[i] << "'" << endl;
				return 1;
			}
		} else if (arg == "-t" || arg == "--time") {
			// устанавливаем время наблюдения
			if (++i == argc) {
				auto cout = threading::cout::lock();
				auto &out = cout.get();
				out << "error: watching time not specified" << endl;
				return 1;
			}
			bool ok = false;
			wg->watchTime = QString::fromUtf8(argv[i]).toULongLong(&ok);
			if (!ok) {
				auto cout = threading::cout::lock();
				auto &out = cout.get();
				out << "error: cannot parse watching time value '" << argv[i] << "'" << endl;
				return 1;
			}
		} else if (arg == "-n" || arg == "--no-console") {
			// отключаем вывод на консоль
			wg->consoleOut = false;
		} else if (arg == "-c" || arg == "--console") {
			// включаем вывод на консоль
			wg->consoleOut = true;
		} else {
			// пытаемся добавить наблюдателя
			if (!wg->watch(arg)) {
				auto cout = threading::cout::lock();
				auto &out = cout.get();
				out << "error: cannot watch for file '" << argv[i] << "'" << endl;
			}
		}
	}

	// теперь нужно дождаться завершения наблюдения
	wg->wait();

	// и успешно выйти
	return 0;
}
