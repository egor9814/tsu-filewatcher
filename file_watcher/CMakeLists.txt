# Имя текущего подпроекта
project(file_watcher)

# Имя библиотеки
set(FILE_WATCHER ${PROJECT_NAME})

# Включаем Position-independent code: https://habr.com/ru/post/181838/
add_compile_options("-fPIC")
# Включаем текущую директорию как include
set(CMAKE_INCLUDE_CURRENT_DIR ON)
# Включаем автоматическое исполнение Qt Metaobject Compiler
set(CMAKE_AUTOMOC ON)

# Нам нужно только ядро из Qt5 (QString, QMap, QFileInfo, ...)
find_package(Qt5 REQUIRED COMPONENTS
        Core
)

# Формируем список файлов с исходным кодом
file(GLOB_RECURSE SOURCE_FILES "src/*.cpp")
# Формируем список заголовочных файлов
file(GLOB_RECURSE HEADER_FILES "include/*.hpp")
# Теперь пусть qt обработает заголовки с наличием Q_OBJECT,
#   и добавит moc_*.cpp файлы к списку исходных кодов
qt_wrap_cpp(SOURCE_FILES ${HEADER_FILES})

# Создаем цель статической библиотеки
add_library(
        ${FILE_WATCHER}
        STATIC
        ${SOURCE_FILES}
)

# Добавляем include пути к библиотеке
target_include_directories(${FILE_WATCHER} PRIVATE include ${THREADING_INCLUDES})

# Связыаем файловый наблюдатель с Qt и потоками
target_link_libraries(${FILE_WATCHER}
        Qt5::Core
        ${THREADING}
)

# Устанавливаем переменные имени и include путей в родительскую область
set(FILE_WATCHER ${FILE_WATCHER} PARENT_SCOPE)
set(FILE_WATCHER_INCLUDES "${CMAKE_CURRENT_SOURCE_DIR}/include" PARENT_SCOPE)
