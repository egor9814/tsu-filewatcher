//
// Created by egor9814 on 13 Nov 2020.
//

#ifndef FILEWATCHER_LOGFILE_FILE_OBSERVER_HPP
#define FILEWATCHER_LOGFILE_FILE_OBSERVER_HPP

#include <file_watcher/file_observer.hpp>
#include <fstream>
#include <mutex>
#include <QString>

namespace fw::file_observers {

	/// Файловый наблюдатель с выводом информации в log-файл, с автозакрытием
	class LogfileFileObserver : public IFileObserver {
		/// Путь до log-файла
		QString mPath;
		/// Файловый поток для логов
		std::fstream mOut;
		/// Мьютех для log-файла
		mutable std::mutex mOutMtx;

	public:
		/** Конструктор
		 * @param path Путь до log-файла
		 * */
		explicit LogfileFileObserver(QString path);

		/// Деструктор
		~LogfileFileObserver() override;

		/** Открыть log-файл
		 * @return true, если удалось открыть на запись, иначе false
		 * */
		bool open();

		/// Закрыть log-файл
		void close();

		/// Проверить открыт ли log-файл
		bool isOpened() const;

		void onFileChanged(const FileStat &oldFileStat, const FileStat &newFileStat) override;
	};

}

#endif //FILEWATCHER_LOGFILE_FILE_OBSERVER_HPP
