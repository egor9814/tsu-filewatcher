//
// Created by egor9814 on 13 Nov 2020.
//

#ifndef FILEWATCHER_FILE_STAT_HPP
#define FILEWATCHER_FILE_STAT_HPP

#include <QString>
#include <QDateTime>
#include <QFileInfo>
#include <memory>

namespace fw {

	/// Перечисление состояний файла
	enum class FileState {
		/// Файл не найден
		NotFound = 0,
		/// Файл изменен
		Modified,
		/// Файл удален
		Removed,
		/// Файл создан
		Created,
		/// Файл добавлен в наблюдение
		Added,
		/// Файл не изменен
		Nothing,
	};

	/// Класс файловой статистики
	class FileStat {
		/// Состояние файла
		FileState mFileState{FileState::NotFound};
		/// Путь до файла
		QString mPath;
		/// Размер файла
		qint64 mSize{0};
		/// Признак существования файла
		bool mExists{false};
		/// Указатель на дату последнего изменения файла
		std::shared_ptr<QDateTime> mLastModified;

	public:
		/// Конструктор по умолчанию
		FileStat() = default;

		/** Конструктор
		 * @param fileState Состояние файла
		 * @param path Путь до файла
		 * @param size Размер файла
		 * @param exists Признак существования файла
		 * @param lastModified Указатель на дату последнего изменения файла
		 * */
		FileStat(FileState fileState, QString path, qint64 size, bool exists,
				 std::shared_ptr<QDateTime> lastModified);

		/// Получить состояние файла
		[[nodiscard]] FileState getFileState() const;

		/// Установить состояние файла
		void setFileState(FileState fileState);

		/// Получить путь до файла
		[[nodiscard]] const QString &getPath() const;

		/// Получить размер файла
		[[nodiscard]] qint64 getSize() const;

		/// Проверить существует ли файл
		[[nodiscard]] bool isExists() const;

		/// Получить дату последнего изменения файла
		[[nodiscard]] const QDateTime &getLastModified() const;

		/// Установить дату последнего изменения файла
		void setLastModified(const QDateTime &lastModified);

		/// Сформировать статистику на основе информации о файле @param fileInfo
		static FileStat fromFileInfo(const QFileInfo &fileInfo);
	};

}

#endif //FILEWATCHER_FILE_STAT_HPP
