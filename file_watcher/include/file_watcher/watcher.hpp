//
// Created by egor9814 on 13 Nov 2020.
//

#ifndef FILEWATCHER_WATCHER_HPP
#define FILEWATCHER_WATCHER_HPP

#include <file_watcher/file_stat.hpp>
#include <memory>
#include <atomic>
#include <mutex>
#include <QObject>
#include <threading/thread.hpp>
#include <threading/thread_safe.hpp>

namespace fw {

	struct IFileObserver;

	/// Наблюдатель за файлами
	class FileWatcher final : public QObject, private threading::Thread {
		/// Класс расширяет QObject, чтобы осуществить связь SIGNAL/SLOT
		Q_OBJECT

		/** Класс расширяет threading::Thread,
		 * чтобы запустить процесс наблюдения в отдельном потоке.
		 * Тип наследования private, потому что в данном классе изменен
		 * механизмы запуска/остановки наблюдения, следовательно нужно скрыть
		 * методы start()/interrupt()
		 * */

	private:
		/// Потоко-безопасный путь до файла
		threading::ThreadSafe<QString> mPath;
		/// Атомарные время периода обновления и общего времени наблюдения
		std::atomic_uint_fast64_t mPeriod, mWatchTime;
		/// Атомарные признаки процесса наблюдения и процесса запуска
		std::atomic_bool mWatching, mStarting;

	protected:
		/// Переопределенный метод, работающий в другом потоке
		void run(threading::InterruptState &interruptState) noexcept(false) override;

	public:
		/** Конструктор
		 * @param path Путь до файла
		 * @param period Период обновления в миллисекундах (минимуальное: 100)
		 * @param watchTime Время наблюдение (значение <= 0 подоразумевает бесконечное время)
		 * */
		FileWatcher(const QString& path, uint64_t period, uint64_t watchTime);

		/** Запустить процесс наблюдения
		 * @return true, если поток запускается впервые, или был полностью завершен, иначе false*/
		bool watch();

		/// Остановить процесс наблюдения
		void stop();

		/// Подождать до полной остановки потока
		void await();

		/// Присоединить интерфейс наблюдателя
		void attach(IFileObserver *observer) const;

		/// Получить путь до наблюдаемого файла
		[[nodiscard]] QString getPath() const;

	signals:
		/** Сигнал для сообщения о том, что статистика изменилась
		 * @param oldFileStat Предыдущая статистика файла
		 * @param newFileStat Текущая статистика файла
		 * */
		void changed(const FileStat &oldFileStat, const FileStat &newFileStat);
	};

}

#endif //FILEWATCHER_WATCHER_HPP
