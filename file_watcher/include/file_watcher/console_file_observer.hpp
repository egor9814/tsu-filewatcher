//
// Created by egor9814 on 13 Nov 2020.
//

#ifndef FILEWATCHER_CONSOLE_FILE_OBSERVER_HPP
#define FILEWATCHER_CONSOLE_FILE_OBSERVER_HPP

#include <file_watcher/file_observer.hpp>

namespace fw::file_observers {

	/// Файловый наблюдатель с выводом на консоль
	class ConsoleFileObserver : public IFileObserver {
	public:
		void onFileChanged(const FileStat &oldFileStat, const FileStat &newFileStat) override;
	};

}

#endif //FILEWATCHER_CONSOLE_FILE_OBSERVER_HPP
