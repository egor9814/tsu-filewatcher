//
// Created by egor9814 on 13 Nov 2020.
//

#ifndef FILEWATCHER_FILE_OBSERVER_HPP
#define FILEWATCHER_FILE_OBSERVER_HPP

namespace fw {

	class FileStat;

	/// Интерфейс файлового наблюдателя
	struct IFileObserver {
		virtual ~IFileObserver() = default;

		/** Метод для обновления информации о файлах
		 * @param oldFileStat Предыдущая статистика файла
		 * @param newFileStat Текущая статистика файла
		 * */
		virtual void onFileChanged(const FileStat &oldFileStat, const FileStat &newFileStat) = 0;
	};

}

#endif //FILEWATCHER_FILE_OBSERVER_HPP
