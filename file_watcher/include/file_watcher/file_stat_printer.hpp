//
// Created by egor9814 on 13 Nov 2020.
//

#ifndef FILEWATCHER_FILE_STAT_PRINTER_HPP
#define FILEWATCHER_FILE_STAT_PRINTER_HPP

#include <iosfwd>

namespace fw {

	class FileStat;

	/// Класс для печати файловой статистики
	class FileStatPrinter {
		/// Класс не может быть инстанцирован
		FileStatPrinter() = default;
		~FileStatPrinter() = default;

	public:
		/** Печать статистики в поток вывода
		 * @param out Поток вывода, например, std::cout
		 * @param fs Файловая статистика
		 * */
		static void printToStream(std::ostream &out, const FileStat &fs);
	};

}

#endif //FILEWATCHER_FILE_STAT_PRINTER_HPP
