//
// Created by egor9814 on 13 Nov 2020.
//

#ifndef FILEWATCHER_FILE_SIZE_UNIT_HPP
#define FILEWATCHER_FILE_SIZE_UNIT_HPP

#include <QString>

namespace fw {

	/// Класс для работы с размером файла
	class FileSizeUnit {
	public:
		/// Перечисление типов размеров
		enum Unit {
			Bytes = 0,
			KBytes,
			MBytes,
			GBytes,
			TBytes,
		};

	private:
		/// Размер файла
		qreal mValue;
		/// Тип размера файла
		Unit mUnit;

	public:
		/** Конструктор
		 * @param value Размер файла
		 * @param unit Тип размера файла
		 * */
		explicit FileSizeUnit(qreal value = 0, Unit unit = Bytes);

		/** Конструктор
		 * @param bytes Размер файла в байтах
		 * */
		explicit FileSizeUnit(qint64 bytes);

		/// Метод для автоопределения типа размера
		FileSizeUnit &autoDetect();

		/// Увеличить тип размера, напрмер KBytes -> MBytes
		FileSizeUnit &up();

		/// Уменьшить тип размера, напрмер MBytes -> KBytes
		FileSizeUnit &down();

		/// Получить размер файла с учетом типа
		[[nodiscard]] const qreal &getValue() const;

		/// Установить размер файла
		void setValue(qreal value);

		/// Получить тип размера
		[[nodiscard]] const Unit &getUnit() const;

		/// Установить тип размера
		void setUnit(Unit unit);

		/// Преобразовать размер файла в строку
		[[nodiscard]] QString toString() const;

		/// Преобразовать размер файла в целое число байтов
		[[nodiscard]] qint64 toBytes() const;
	};

}

#endif //FILEWATCHER_FILE_SIZE_UNIT_HPP
