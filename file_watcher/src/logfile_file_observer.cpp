//
// Created by egor9814 on 13 Nov 2020.
//

#include <file_watcher/logfile_file_observer.hpp>
#include <file_watcher/file_stat.hpp>
#include <file_watcher/file_stat_printer.hpp>
#include <QDateTime>
#include <utility>

namespace fw::file_observers {

	LogfileFileObserver::LogfileFileObserver(QString path) : mPath(std::move(path)) {}

	LogfileFileObserver::~LogfileFileObserver() {
		// закрываем файл во время разрушения объекта
		close();
	}

	bool LogfileFileObserver::open() {
		// пытаемся открыть файл
		if (isOpened())
			// возвращаем false, т.к. файл уже успешно открыт
			return false;
		{
			// синхронизирумся
			std::lock_guard<decltype(mOutMtx)> lock{mOutMtx};
			// открываем файл
			mOut.open(mPath.toStdString(), std::ios_base::app);
		}
		// возвращаем статус открытия
		return isOpened();
	}

	void LogfileFileObserver::close() {
		// закрываем файл, если только он открыт
		if (isOpened()) {
			// синхронизируемся
			std::lock_guard<decltype(mOutMtx)> lock{mOutMtx};
			// закрываем
			mOut.close();
		}
	}

	bool LogfileFileObserver::isOpened() const {
		// синхронизируемся
		std::lock_guard<decltype(mOutMtx)> lock{mOutMtx};
		// получаем и возвращаем стстаус
		return mOut.is_open();
	}

	void LogfileFileObserver::onFileChanged(const FileStat &, const FileStat &newFileStat) {
		// если файл закрыт, то делать нечего, => выходим
		if (!isOpened())
			return;
		// синхронизируемся
		std::lock_guard<decltype(mOutMtx)> lock{mOutMtx};
		// получаем текущее время
		auto now = QDateTime::currentDateTime();
		// выводим время в формате "год.месяц.день час:минута:секунда.миллисекунда"
		mOut << now.toString("yyyy.MM.dd hh:mm:ss.zzz ").toStdString();
		// выводим статистику
		FileStatPrinter::printToStream(mOut, newFileStat);
		// и новую строку
		mOut << std::endl;
	}

}
