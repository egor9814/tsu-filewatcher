//
// Created by egor9814 on 13 Nov 2020.
//

#include <file_watcher/file_size_unit.hpp>

namespace fw {

	FileSizeUnit::FileSizeUnit(qreal value, FileSizeUnit::Unit unit)
			: mValue(value), mUnit(unit) {}

	FileSizeUnit::FileSizeUnit(qint64 bytes) : FileSizeUnit(static_cast<qreal>(bytes)) {}

	FileSizeUnit &FileSizeUnit::autoDetect() {
		while (mValue >= 1024.0 && mUnit != TBytes) {
			// пока размер файла превышает 1024 mUnit и mUnit < террабайтов, увеличиваем тип размера
			up();
		}
		while (mValue < 1.0 && mUnit != Bytes) {
			// пока размер файла меньше 1 mUnit и mUnit > байтов, уменьшаем тип размера
			down();
		}
		return *this;
	}

	FileSizeUnit &FileSizeUnit::up() {
		if (mUnit != TBytes) {
			// просто делим размер на 1024 и увеличиваем тип размера
			mValue /= 1024.0;
			mUnit = static_cast<Unit>(mUnit + 1);
		}
		return *this;
	}

	FileSizeUnit &FileSizeUnit::down() {
		if (mUnit != Bytes) {
			// просто умнажаем размер на 1024 и уменьшаем тип размера
			mValue *= 1024.0;
			mUnit = static_cast<Unit>(mUnit - 1);
		}
		return *this;
	}

	const qreal &FileSizeUnit::getValue() const {
		return mValue;
	}

	void FileSizeUnit::setValue(qreal value) {
		mValue = value;
	}

	const FileSizeUnit::Unit &FileSizeUnit::getUnit() const {
		return mUnit;
	}

	void FileSizeUnit::setUnit(FileSizeUnit::Unit unit) {
		mUnit = unit;
	}

	QString FileSizeUnit::toString() const {
		// строка с буквенным представлением типа размера
		static auto unitNames = " KMGT";
		// буфер для "человеческого" размера файла
		char unitStr[3]{0};
		// индекс, куда расположить обозначение байтов 'B'
		uint unitB = 0;
		if (mUnit != Bytes) {
			// если тип не байты, то установим необходимую букву в начло...
			unitStr[0] = unitNames[static_cast<uint>(mUnit)];
			// ... и увеличим индекс расположения 'B'
			unitB++;
		}
		// установим 'B' в нужное место
		unitStr[unitB] = 'B';
		// теперь преобразуем размер в строку с точностью до 2 знаков и допишем тип
		return QString::number(mValue, 'f', 2) + unitStr;
	}

	qint64 FileSizeUnit::toBytes() const {
		// сделаем копию, чтобы не повредить текущий объект
		auto size(*this);
		// уменьшаем тип до байтов
		while (size.mUnit != Bytes) {
			size.down();
		}
		// возвращаем целое число байтов
		return static_cast<qint64>(size.mValue);
	}

}
