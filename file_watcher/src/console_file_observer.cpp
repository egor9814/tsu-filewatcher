//
// Created by egor9814 on 13 Nov 2020.
//

#include <file_watcher/console_file_observer.hpp>
#include <file_watcher/file_stat_printer.hpp>
#include <threading/safe_cout.hpp>
#include <iostream>

namespace fw::file_observers {
	void ConsoleFileObserver::onFileChanged(const FileStat &, const FileStat &newFileStat) {
		// блокируем std::cout
		auto cout = threading::cout::lock();
		auto &out = cout.get();
		// выводим статистику
		FileStatPrinter::printToStream(out, newFileStat);
		// и переход на новую строку
		out << std::endl;
	}
}
