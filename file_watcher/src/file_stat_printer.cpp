//
// Created by egor9814 on 13 Nov 2020.
//

#include <file_watcher/file_stat_printer.hpp>
#include <file_watcher/file_stat.hpp>
#include <file_watcher/file_size_unit.hpp>
#include <iostream>

namespace fw {

	void FileStatPrinter::printToStream(std::ostream &out, const FileStat &fs) {
		// выводим путь файла
		out << "file '" << fs.getPath().toStdString() << "' ";
		// признак вывода времени последнего изменения
		bool printTime = false;
		// признак вывода размера
		bool printSize = false;
		// проверяем состояния
		switch (fs.getFileState()) {
			case FileState::NotFound:
				// файл не найден
				out << "not found";
				break;

			case FileState::Modified:
				// файл изменен
				out << "modified";
				// выведем также время и размер
				printTime = true;
				printSize = true;
				break;

			case FileState::Removed:
				// файл удален
				out << "removed";
				// выведем только время
				printTime = true;
				break;

			case FileState::Created:
				// файл создан
				out << "created";
				// выведем время создания и размер
				printTime = true;
				printSize = true;
				break;

			case FileState::Added:
				// файл добавлен в наблюдение
				out << "added to watching";
				// выведем время создания и размер
				printTime = true;
				printSize = true;
				break;

			case FileState::Nothing:
				// файл не изменен
				out << "unchanged";
				break;

			default:
				// если будет добавлено новое состояние, то произойдет ошибка в рантайме
				Q_ASSERT_X(false, __FUNCTION__, "unimplemented file state");
				break;
		}
		// выводим время
		if (printTime) {
			out << " (date: "
				<< fs.getLastModified().toString("dd.MM.yyyy hh:mm:ss").toStdString()
				<< ')';
		}
		// выводим размер
		if (printSize) {
			out << " (size: "
				<< FileSizeUnit(fs.getSize()).autoDetect().toString().toStdString()
				<< ')';
		}
	}

}
