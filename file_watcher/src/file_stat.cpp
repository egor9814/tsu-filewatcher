//
// Created by egor9814 on 13 Nov 2020.
//

#include <file_watcher/file_stat.hpp>
#include <utility>

namespace fw {

	FileStat::FileStat(FileState fileState, QString path, qint64 size, bool exists,
					   std::shared_ptr<QDateTime> lastModified)
			: mFileState(fileState),
			  mPath(std::move(path)),
			  mSize(size),
			  mExists(exists),
			  mLastModified(std::move(lastModified)) {}

	FileState FileStat::getFileState() const {
		return mFileState;
	}

	void FileStat::setFileState(FileState fileState) {
		mFileState = fileState;
	}

	const QString &FileStat::getPath() const {
		return mPath;
	}

	qint64 FileStat::getSize() const {
		return mSize;
	}

	bool FileStat::isExists() const {
		return mExists;
	}

	const QDateTime &FileStat::getLastModified() const {
		return *mLastModified;
	}

	void FileStat::setLastModified(const QDateTime &lastModified) {
		mLastModified.reset(new QDateTime(lastModified));
	}

	FileStat FileStat::fromFileInfo(const QFileInfo &fileInfo) {
		FileStat stat;
		stat.mPath = fileInfo.absoluteFilePath();
		stat.mExists = fileInfo.exists();
		if (stat.mExists) {
			// если файл существует, то устанавливаем свойства существющего файла:
			// 1) состояние Nothing, т.к. мы не не можем сказать что он каким-либо образом изменен
			stat.mFileState = FileState::Nothing;
			// 2) дату последнего изменения
			stat.setLastModified(fileInfo.lastModified());
			// 3) размер файла
			stat.mSize = fileInfo.size();
		}
		return stat;
	}

}
