//
// Created by egor9814 on 13 Nov 2020.
//

#include <file_watcher/watcher.hpp>
#include <file_watcher/file_observer.hpp>
#include <chrono>

namespace fw {

	/// Функция для получения текущего системного времени в миллисекундах
	uint64_t currentMS() {
		using namespace std::chrono;
		// получаем текущее время
		auto now = high_resolution_clock::now();
		// преобразуем в миллисекунды
		auto ms = duration_cast<milliseconds>(now.time_since_epoch()).count();
		// возвращаем
		return ms;
	}

	/** Функция для проверки файла на модификации
	 * @brief Эта функция вызывается только в случае если файл существует
	 * @param o Предыдущая статистика
	 * @param n Текущая статистика
	 * */
	void checkModifications(const FileStat &o, FileStat &n) {
		// получаем время
		const auto &d1 = o.getLastModified(), &d2 = n.getLastModified();
		// получаем размер
		auto s1 = o.getSize(), s2 = n.getSize();
		if (s1 != s2 || d1 != d2) {
			// если размеры или время не совпадают, то файл был изменен
			n.setFileState(FileState::Modified);
		}
	}

	/** Функция для обновления статистики файла
	 * @param old Предыдущая статистика
	 * @return Текущую статистику */
	FileStat updateStat(const FileStat &old) {
		QFileInfo info(old.getPath());
		// формируем начальную статистику
		auto stat = FileStat::fromFileInfo(info);
		// метод stat.getFileState() может вернуть только FileState::NotFound или FileState::Nothing
		// признак того, что сейчас файл не существует (не найден)
		auto notFound = stat.getFileState() == FileState::NotFound;
		// пройдемся по состоянию предыдущей статистики
		switch (old.getFileState()) {
			// файл не найден или удален
			case FileState::NotFound:
			case FileState::Removed: {
				if (notFound)
					// файл и сейчас не существует, значит ничего не изменилось
					stat.setFileState(FileState::Nothing);
				else
					// файл "появился", значит его создали
					stat.setFileState(FileState::Created);
				break;
			}

			// файл создан или изменен
			case FileState::Created:
			case FileState::Modified: {
				if (notFound)
					// файл не существует, значит его удалили
					stat.setFileState(FileState::Removed);
				else
					// файл существует, значит проверим на изменения
					checkModifications(old, stat);
				break;
			}

			// файл не менялся
			case FileState::Nothing: {
				// существовал ли файл до этого?
				auto exists = old.isExists();
				if (notFound) {
					// файл не найден, значит...
					if (exists) {
						// если файл существовал до текущего момента, то его удалили
						stat.setFileState(FileState::Removed);
						// установим время удаления как текущее время
						stat.setLastModified(QDateTime::currentDateTime());
					}
					else
						// если не существовал, то ничего не изменилось
						stat.setFileState(FileState::Nothing);
				} else {
					// файл найден, значит...
					if (exists)
						// если файл существовал до текущего момента, то проверим на изменения
						checkModifications(old, stat);
					else
						// если не существовал, то значит его создали
						stat.setFileState(FileState::Created);
				}
				break;
			}
		}
		// возвращаем сформированную статистику
		return stat;
	}

	void FileWatcher::run(threading::InterruptState &) noexcept(false) {
		// специальная структура, которая изменит флаг процесса наблюдения, по завершении потока
		struct stop_watching {
			std::atomic_bool &watching;
			~stop_watching() {
				watching = false;
			}
		} _stop_watching_{mWatching};

		// начали процесс наблюдения
		mWatching = true;
		// запуск завершен
		mStarting = false;
		// вдруг кто-то решит остоновить наблюдение сейчас, проверим это
		if (!mWatching)
			return;

		// получаем путь к файлу
		auto path = getPath();
		// формируем начальную статистику
		auto oldStat = FileStat::fromFileInfo(QFileInfo(path));
		// вдруг кто-то решит остоновить наблюдение сейчас, проверим это
		if (!mWatching)
			return;
		if (oldStat.getFileState() != FileState::NotFound) {
			// если файл существует, то значит он добавлен в наблюдение
			oldStat.setFileState(FileState::Added);
		}
		// сигнализируем о начальном состоянии файла
		emit changed(oldStat, oldStat);

		// получаем период обновления
		const auto &period = mPeriod.load();
		// вспомогательная лямбда-функция для одной итерации цикла проверки
		auto loopBody = [this, &period, &oldStat]() {
			// получаем новую статистику
			auto stat = updateStat(oldStat);
			// проверим состояиние
			switch (stat.getFileState()) {
				case FileState::Nothing:
					// если файл не изменился, то незачем лишний раз вызвать changed(...)
					break;

				default:
					// вызваем сигнал об изменении
					emit changed(oldStat, stat);
					break;
			}
			// обновляем статистику
			oldStat = stat;
			// спим указанный period
			std::this_thread::sleep_for(std::chrono::milliseconds(period));
		};

		// получаем время наблюдения
		auto watchTime = mWatchTime.load();
		if (watchTime <= 0) {
			// наюлюдаем всегда
			while (mWatching) {
				loopBody();
			}
		} else {
			// сформируем время наблюдения:
			// получаем текущее время
			auto now = currentMS();
			// формируем время окончания (*1000, потому что watchTime в секундах
			auto end = now + watchTime * 1000;
			// наблюдаем пока не закончилось время
			while (mWatching && now <= end) {
				loopBody();
				// получаем новое текущее время
				now = currentMS();
			}
		}
	}

	FileWatcher::FileWatcher(const QString& path, uint64_t period, uint64_t watchTime)
			: mPath(path),
			  mPeriod(period),
			  mWatchTime(watchTime),
			  mWatching(false),
			  mStarting(false) {
		// на всякий случай установим минимальное время обновления
		if (mPeriod < 100)
			mPeriod = 100;
	}

	bool FileWatcher::watch() {
		// если процесс наблюдения уже идет или только запускается, то вернем false
		if (mWatching || mStarting)
			return false;
		// говорим, что процесс запускается
		mStarting = true;
		// пытемся запустить поток
		return start();
	}

	void FileWatcher::stop() {
		// остонавливаем процесс наблюдения и запуска
		mWatching = false;
		mStarting = false;
	}

	void FileWatcher::await() {
		// ждем завершения потока
		if (mWatching || mStarting)
			Thread::wait();
	}

	void FileWatcher::attach(IFileObserver *observer) const {
		// присоединяем слот лямда-функции к сигналу changed(...), потому что
		// по какой-то неизвестной причине сигнал, вызванный из другого потока, не сообщает
		// привязонному слоту таким образом
		//   `connect(this, &FileWatcher::changed, observer, &IFileObserver::onFileChanged);`
		// , но если обернуть вызов в лямбда функцию, то все работает замечательно
		connect(this, &FileWatcher::changed, [observer](const FileStat &a, const FileStat &b) {
			observer->onFileChanged(a, b);
		});
	}

	QString FileWatcher::getPath() const {
		// блокируем путь
		auto lock = mPath.lock();
		// возвращаем значение
		return lock.get();
	}

}
