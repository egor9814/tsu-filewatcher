//
// Created by egor9814 on 15 Nov 2020.
//

#include <threading/safe_cout.hpp>
#include <iostream>

namespace threading {

	/// Структура содержащая ссылку на поток вывода
	struct out_holder {
		/// Поток вывода
		std::ostream &out;
		/// Мьютекс
		std::mutex mtx;

		/// Ссылка на std::cout
		static out_holder &cout() {
			static out_holder out{
				std::cout
			};
			return out;
		}
	};

	AutoLock<std::ostream> cout::lock() {
		// получаем ссылку std::cout
		auto &out = out_holder::cout();
		// возвращаем автоблокировщик
		return AutoLock(out.out, out.mtx);
	}

}