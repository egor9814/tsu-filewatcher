//
// Created by egor9814 on 15 Nov 2020.
//

#include <threading/thread.hpp>

namespace threading {

	InterruptException::InterruptException() : runtime_error("InterruptException") {}


	InterruptState::InterruptState(bool state) : mState(state) {}

	void InterruptState::interrupt() {
		mState = true;
	}

	InterruptState::InterruptState(InterruptState &&is) noexcept : InterruptState(is.mState) {
		is.mState = false;
	}

	InterruptState &InterruptState::operator=(InterruptState &&is) noexcept {
		if (this != &is) {
			auto self = mState.load();
			auto other = is.mState.load();
			mState = other;
			is.mState = self;
		}
		return *this;
	}

	bool InterruptState::isInterrupted() const {
		return mState.load();
	}

	void InterruptState::check() const noexcept(false) {
		if (isInterrupted())
			throw InterruptException();
	}


	void Thread::worker(Thread *thread) {
		try {
			// пытаемся выполнить метод run
			thread->run(thread->mState);
		} catch (const InterruptException &ie) {
			// если поток был прерван, то установим данное исключение
			thread->mInterrupt = ie;
		} catch (...) {
			// также могут быть другие исключения, но оставим их на совести пользователя
			throw;
		}
	}

	void Thread::run(InterruptState &interruptState) noexcept(false) {
		// проверим, вдруг нас прервали
		interruptState();
		if (mRunnable)
			// если существует "бегущий", то выполним его
			mRunnable->run(interruptState);
		// проверим, вдруг нас прервали
		interruptState();
	}

	Thread::Thread(IRunnablePtr runnable) : mRunnable(std::move(runnable)) {}

	Thread::Thread(Thread &&t) noexcept : Thread() {
		swap(t);
	}

	Thread &Thread::operator=(Thread &&t) noexcept {
		if (this != &t) {
			// при перемещении потоков, текущий неообходимо остановить
			try {
				interrupt();
			} catch (const InterruptException &) {
				// ничего не делаем
			} catch (...) {
				throw;
			}
			mRunnable.reset();
			mInterrupt.reset();
			swap(t);
		}
		return *this;
	}

	Thread::~Thread() {
		// ожидаем корректного завершения потока
		wait();
	}

	void Thread::interrupt() noexcept(false) {
		if (isAlive()) {
			// если поток запущен, то остановим его
			mState.interrupt();
			// подождем завершения
			wait();
			// если он завершился из-за interrupt(), то сгенирируем соответсвующее исключение
			if (mInterrupt)
				throw InterruptException(*mInterrupt);
		}
	}

	bool Thread::start() {
		// если указатель на поток не пуст, значит он уже был запущен
		if (mThread)
			return false;
		// создаем поток
		mThread = std::make_shared<std::thread>(worker, this);
		return true;
	}

	bool Thread::isAlive() const {
		// если поток существует и к нему можно присоединится, то поток работает (жив)
		return mThread && mThread->joinable();
	}

	void Thread::wait() {
		if (isAlive()) {
			// если поток жив, то присоединяемся к нему
			mThread->join();
			// после чего удаляем его, чтобы можно было посзже перезапустить
			mThread.reset();
		}
	}

	void Thread::swap(Thread &t) {
		std::swap(mRunnable, t.mRunnable);
		std::swap(mThread, t.mThread);
		std::swap(mState, t.mState);
		std::swap(mInterrupt, t.mInterrupt);
	}

}
