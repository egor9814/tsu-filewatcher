//
// Created by egor9814 on 15 Nov 2020.
//

#ifndef THREADING_THREAD_SAFE_HPP
#define THREADING_THREAD_SAFE_HPP

#include <mutex>

namespace threading {

	/// Класс автоблокировки мьютекса, для получения синхронизированных данных
	template <typename T, typename MutexType = std::mutex>
	class AutoLock {
	public:
		using type = T;
		using mutex_type = MutexType;

	private:
		/// Ссылка на значение
		type &mValueRef;
		/// Ссылка на мьютекс
		mutex_type &mMutexRef;

	public:
		/** Конструктор
		 * @param valueRef Ссылка на значение
		 * @param mutexRef Ссылка на мьютекс
		 * */
		AutoLock(type &valueRef, mutex_type &mutexRef) : mValueRef(valueRef), mMutexRef(mutexRef) {
			// блокируем мьютекс
			mMutexRef.lock();
		}

		/// Экземпляр данного класса нельзя ни переместить, ни скопировать
		AutoLock(const AutoLock &) = delete;
		AutoLock(AutoLock &&) noexcept = delete;
		AutoLock &operator=(const AutoLock &) = delete;
		AutoLock &operator=(AutoLock &&) noexcept = delete;

		~AutoLock() {
			// разблокировывем мьютекс
			mMutexRef.unlock();
		}

		/// получить ссылку на значение
		type &get() const {
			return mValueRef;
		}
	};

	/// Класс потоко-безопасного объекта
	template <typename T, typename MutexType = std::mutex>
	class ThreadSafe {
	public:
		using type = T;
		using mutex_type = MutexType;
		using lock_type = AutoLock<type, mutex_type>;
		using const_lock_type = AutoLock<const type, mutex_type>;

	private:
		/// Объект
		type mValue;
		/// Мьютекс
		mutable mutex_type mMtx;

	public:
		/** Конструктор
		 * @param value Защищаемы объект
		 * */
		explicit ThreadSafe(const type &value) : mValue(value) {}

		/// Экземпляр данного класса нельзя ни переместить, ни скопировать
		ThreadSafe(const ThreadSafe &) = delete;
		ThreadSafe(ThreadSafe &&) noexcept = delete;
		ThreadSafe &operator=(const ThreadSafe &) = delete;
		ThreadSafe &operator=(ThreadSafe &&) noexcept = delete;

		/// Блокировка на доступ
		lock_type lock() {
			return lock_type(mValue, mMtx);
		}

		/// Блокировка на доступ
		const_lock_type lock() const {
			return const_lock_type(mValue, mMtx);
		}

	};

}

#endif //THREADING_THREAD_SAFE_HPP
