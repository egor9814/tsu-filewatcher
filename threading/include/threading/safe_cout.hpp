//
// Created by egor9814 on 15 Nov 2020.
//

#ifndef THREADING_SAFE_COUT_HPP
#define THREADING_SAFE_COUT_HPP

#include <threading/thread_safe.hpp>
#include <iosfwd>

namespace threading {

	/// Структура потоко-безопасного std::cout
	struct cout {
		/// Метод для автоблокировки std::cout
		static AutoLock<std::ostream> lock();

	private:
		/// Структура не может быть инстанцирована
		cout() = default;
		~cout() = default;
	};

}

#endif //THREADING_SAFE_COUT_HPP
