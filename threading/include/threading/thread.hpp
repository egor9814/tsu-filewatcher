//
// Created by egor9814 on 15 Nov 2020.
//

#ifndef THREADING_THREAD_HPP
#define THREADING_THREAD_HPP

#include <atomic>
#include <stdexcept>
#include <memory>
#include <optional>
#include <thread>

namespace threading {

	/// Класс-исключение для экстренного прерывания потока
	class InterruptException : public std::runtime_error {
	public:
		InterruptException();
	};

	/// Класс состояния прерывания
	class InterruptState {
		friend class Thread;

		/// Текущее состояние прерывания
		std::atomic_bool mState;

		explicit InterruptState(bool state);

		/// Прервать
		void interrupt();

	public:
		InterruptState(const InterruptState &) = delete;
		InterruptState &operator=(const InterruptState &) = delete;

		InterruptState(InterruptState &&) noexcept;
		InterruptState &operator=(InterruptState &&) noexcept;

		/// Проверить, прерван ли поток
		[[nodiscard]] bool isInterrupted() const;

		/** Метод, генерирующий исключение InterruptException,
		 * если isInterrupted() == true
		 * */
		void check() const noexcept(false);

		/// Тоже самое, что и check()
		void operator()() const noexcept(false) { check(); }
	};

	/// Интерфейс "бегущего" (исполнителя)
	struct IRunnable {
		virtual ~IRunnable() = default;

		/// Метод для исполнения с учетом состояния прерывания @param interruptState
		virtual void run(InterruptState &interruptState) noexcept(false) = 0;
	};
	using IRunnablePtr = std::shared_ptr<IRunnable>;

	/// Класс потока
	class Thread {
		/// Указатель на "бегущего"
		IRunnablePtr mRunnable;
		/// Указатель на поток
		std::shared_ptr<std::thread> mThread;
		/// Состояние прерывания
		InterruptState mState{false};
		/// Исключение, вызванное InterruptState::check()
		std::optional<InterruptException> mInterrupt;

		/// Метод, работающий в другом потоке
		static void worker(Thread *thread);

	protected:
		/** Переопределяемый(виртуальный) метод для исполнения в другом потоке,
		 * с учетом состояния прерывания @param interruptState
		 * */
		virtual void run(InterruptState &interruptState) noexcept(false);

	public:
		/** Конструктор
		 * @param runnable Указатель на "бегущего"
		 * */
		explicit Thread(IRunnablePtr runnable = nullptr);

		/// Поток нельзя скопировать
		Thread(const Thread &) = delete;
		Thread &operator=(const Thread &) = delete;

		Thread(Thread &&t) noexcept;

		Thread &operator=(Thread &&t) noexcept;

		~Thread();

		/// Прервать исполнение потока
		void interrupt() noexcept(false);

		/// Запустить поток
		bool start();

		/// Проверить, жив ли поток (работает ли)
		[[nodiscard]] bool isAlive() const;

		/// Ожидать завершения потока
		void wait();

		/// Поменять потоки местами
		void swap(Thread &t);
	};
}

#endif //THREADING_THREAD_HPP
