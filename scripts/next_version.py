""" Генератор версии """

import json
import os
import sys


class Version:
    """ Класс версии программы по приципу semver.org """

    def __init__(self, major, minor, patch):
        """ Конструктор """
        self.major = major
        self.minor = minor
        self.patch = patch

    def next_major(self):
        """ Увеличить значение major """
        self.major += 1

    def next_minor(self):
        """ Увеличить значение minor """
        self.minor += 1

    def next_patch(self):
        """ Увеличить значение patch """
        self.patch += 1

    def __str__(self):
        """ Преобразовать в строку """
        return f'{self.major}.{self.minor}.{self.patch}'

    def __repr__(self):
        """ Репрезентовать в строку """
        return f'Version({self.major}, {self.minor}, {self.patch})'

    def to_cpp(self):
        """ Преобразовать в C++ шаблон """
        res = "/* Auto-generated file by next_version.py */\n"
        res += "#ifndef VERSION_HPP\n#define VERSION_HPP\n"
        res += "#define FW_VERSION_MAJOR " + str(self.major) + "\n"
        res += "#define FW_VERSION_MINOR " + str(self.minor) + "\n"
        res += "#define FW_VERSION_PATCH " + str(self.patch) + "\n"
        res += "#endif //VERSION_HPP"
        return res

    def to_json(self):
        """ Преобразовать в JSON шаблон """
        return json.dumps({
            'major': self.major,
            'minor': self.minor,
            'patch': self.patch
        })

    @staticmethod
    def from_json(s):
        """ Преобразовать из JSON """
        d = json.loads(s)
        return Version(d['major'], d['minor'], d['patch'])


def parse_version(p):
    """ Преобразовать файл в Version """
    if not os.path.isfile(p):
        return Version(0, 0, 0)
    s = ""
    with open(p) as f:
        # читаем файл
        s = f.read()
        # закрываем
        f.close()
    # формируем версию из JSON
    return Version.from_json(s)


def save_version_cpp(p, v):
    """ Сохраняем C++ файл """
    with open(p, 'w') as f:
        f.write(v.to_cpp())
        f.close()


def save_version_json(p, v):
    """ Сохраняем JSON файл """
    with open(p, 'w') as f:
        f.write(v.to_json())
        f.close()


def main(i, o, args):
    # парсим верию из JSON
    v = parse_version(i)
    # печатаем её
    print(f'current version: {v}')
    # проходимся по командам
    for cmd in args:
        if cmd == "patch" or cmd == "p":
            # увеличить patch
            v.next_patch()
        elif cmd == "minor" or cmd == "m":
            # увеличить minor
            v.next_minor()
        elif cmd == "major" or cmd == "M":
            # увеличить major
            v.next_major()
        elif cmd == "reset" or cmd == "r":
            # сбросить
            v = Version(0, 0, 0)
    # печатаем новую версию
    print(f'new version: {v}')
    # сохраняем
    save_version_cpp(o, v)
    save_version_json(i, v)


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("input .json version and output .hpp version not specified")
        exit(1)
    else:
        main(sys.argv[1], sys.argv[2], sys.argv[3:])
